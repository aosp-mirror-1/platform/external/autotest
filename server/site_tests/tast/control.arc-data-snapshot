# Copyright 2021 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = 'Chromium OS team'
NAME = 'tast.arc-data-snapshot'
TIME = 'LONG'
TEST_TYPE = 'Server'
ATTRIBUTES = 'suite:arc-data-snapshot_per-build'
MAX_RESULT_SIZE_KB = 50 * 1024
PY_VERSION = 3

# tast.py uses binaries installed from autotest_server_package.tar.bz2.
REQUIRE_SSP = True

DOC = '''
Run arc-data-snapshot Tast tests.

Tast is an integration-testing framework analagous to the test-running portion
of Autotest. See https://chromium.googlesource.com/chromiumos/platform/tast/ for
more information.

This test runs Tast-based ARC data/ snapshot related tests against a remote DUT.
This is a smoke test and verifies a whole ARC data/ snapshot flow, that requires
several reboots, app downloads and takes a considerable amount of time to be
added to the existing test suites.
'''

def run(machine):
    job.run_test('tast',
                 host=hosts.create_host(machine), max_run_sec=10800,
                 test_exprs=['("group:arc-data-snapshot")'], command_args=args)

parallel_simple(run, machines)
