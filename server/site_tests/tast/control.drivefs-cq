# Copyright 2021 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = 'chromeos-files-syd@google.com'
NAME = 'tast.drivefs-cq'
TIME = 'MEDIUM'
TEST_TYPE = 'Server'
ATTRIBUTES = 'suite:drivefs-cq'
MAX_RESULT_SIZE_KB = 50 * 1024
PY_VERSION = 3

# tast.py uses binaries installed from autotest_server_package.tar.bz2.
REQUIRE_SSP = True

DOC = '''
Runs all the tests that have the tast drivefs-cq attribute. These tests are ran
when a new DriveFS uprev CL is submitted.

Tast is an integration-testing framework analagous to the test-running portion
of Autotest. See https://chromium.googlesource.com/chromiumos/platform/tast/ for
more information.
'''

def run(machine):
    job.run_test('tast',
                 host=hosts.create_host(machine),
                 test_exprs=['("group:drivefs-cq")'],
                 ignore_test_failures=False, max_run_sec=3600,
                 command_args=args,
                 clear_tpm=True,
                 retries=2)

parallel_simple(run, machines)
